//IIFE = Immediate invoked function expression
var LIB1={};
(function(){
    LIB1.a='something';

    LIB1.getDetails=function(name,surname){
        return `${name} ${surname}`;
    }
    
    document.addEventListener('DOMContentLoaded', ()=>{
        console.log(LIB1.getDetails('Gigel','Ionel'));
    })
    
    console.log(LIB1.getDetails('Gigel','Ionel'));
}());

