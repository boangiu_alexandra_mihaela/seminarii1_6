setTimeout(()=>{
    console.log('Handler timeout');
    const button = document.querySelector('#my-btn');
    button.addEventListener('click', (event)=>{
    console.log(event.currentTarget);
});
},1000);

window.addEventListener('load',()=>{
    console.log('Handler load');
    const button = document.querySelector('#my-btn');
    button.addEventListener('click', (event)=>{
    console.log(event.currentTarget);
});
})

document.addEventListener('DOMContentLoaded',()=>{
    console.log('Handler DOM loaded');
    const button = document.querySelector('#my-btn');
    button.addEventListener('click', (event)=>{
    console.log(event.currentTarget);
});
});