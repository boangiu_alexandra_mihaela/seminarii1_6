/**function func1(){
    return 5;
}
*/
const func1=()=>5;
console.log(func1());

/**
 function func2(a){
     return a+1;
 }
 */

 const func2=a=>a+1;
 console.log(func2(3));


/**
 function func3(a,b){
     return a+b;
 }
 */
const func3=(a,b) =>a+b;
console.log(func3(3,4));

const func4=(a,b) =>{
    console.log('Intermediate operation');
    return a+b;
}

console.log(func4(3,4));

const func5=(a,b) =>{
    return{a,b};
}
console.log(func5(3,4));
