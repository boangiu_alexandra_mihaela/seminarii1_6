function authenticate(username, password){
    return new Promise((resolve, reject)=>{
        if(username === 'admin' && password === 'pass')
        {
            resolve({
                auth_token:"SECRET", // "auth_token":"SECRET",
                expire_time :1999,
                role: "ADMIN"
            })
        }
        else{
            reject('Invalid Credentials');
        }
    });
}

function redirect(userData, path){
    return new Promise((resolve, reject) =>{
        if(!userData.role){
            reject('Unauthorized');
        }else{
            if(path.includes('admin')){
                resolve('Hello Master')
            }else{
                resolve('Hello User')
            }
        }
    });
}

var a=5;
/**(async function(){
    const userData=authenticate('admin', 'pass');
    console.log(userData);
})()*/
//IIFE - Immediate Invoked Function Expression
/**(async function(){
    const b=authenticate('admin', 'pass');
    console.log(b);
})()*/
(async function(){
    const userData=authenticate('admin', 'pass');
    const message =await redirect(userData, '/home');
    alert(message);
})()