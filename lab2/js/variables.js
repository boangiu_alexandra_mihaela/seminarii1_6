/* var a = 10;
console.log(a);
console.log(typeof a);
a = 'something';
console.log(a);
console.log(typeof a);
a = true;
console.log(a);
console.log(typeof a);
a = undefined; // Never do this
console.log(a);
console.log(typeof a);
a = null;
console.log(a);
console.log(typeof a);
a = Symbol('unique');
console.log(a);
console.log(typeof a);
a = BigInt(10);
console.log(a);
console.log(typeof a); */

/* var a = 10;
var b = undefined; // Never do this; undefined se foloseste doar de engine-ul de JavaScript
var c = null;
console.log(a);
console.log(b);
console.log(c);
console.log(b==c); // cohersion = uneia dintre cele 2 variabile i se face cast la valoarea celeilalte variabile
console.log(b===c); // am valori egale, doar ca triple equals verifica si tipul de date */

/* var x = 'something'; // primitiva string (string literals)
var y = String('something'); // primitiva string
var z = new String('something'); // obiect (secventa de caractere)
console.log(x);
console.log(y);
console.log(z);
console.log(x==y);
console.log(x==z);
console.log(y==z);
console.log('x si y', x===y);
console.log('x si z', x===z);
console.log('y si z', y===z);
console.log(typeof z); */

function simpleFunction() {
    console.log('Call from a simple function');
}

simpleFunction();