var person = {}; // new Object() => object literals
person.name = 'John';
person.surname = 'Doe';
console.log(person); // Object-type

var person1 = {};
person1.nume = 'Jane';
person1.prenume = 'Doe';
console.log(person1);

function Person(name, surname) {
    this.name = name;
    this.surname = surname;
}

Person.prototype.getDetailsNormal = function() {
    return this.name + ' ' + this.surname;
}

Person.prototype.getDetailsInterpolation = function() {
    return `${this.name} ${this.surname}`;
}

var person2 = new Person('Gigel', 'Popel');
console.log(person2); // Person-type
console.log(person2.getDetailsNormal());
console.log(person2.getDetailsInterpolation());


function Student(name, surname, group) {
    Person.call(this,name,surname); // fac binding intre this-ul din student (cand se apeleaza constructorul Person sa stie ca this-ul vine de la un obiect de tip Student)
    this.group = group;
}
Student.prototype = Object.create(Person.prototype);
Object.defineProperty(Student.prototype,'constructor', {
    value: Student,
    enumerable: false,
    writable: true
});
console.log(Student.prototype.constructor);

Student.prototype.studentFactory = function(university) {
    function signIn() {
        console.log(`${this.name} ${this.surname} signed in to the university: ${university}`);
    }

    function SiSC() {
        if (university === 'ASE') {
            console.log(`${this.name} ${this.surname} is doing volunteering in SiSC organization.`);
        }
        else {
            console.log(`Too bad that ${this.name} ${this.surname} is studying at: ${university}. You can't join SiSC.`);
        }
    }

    function graduate() {
        console.log(`${this.name} ${this.surname} graduated university: ${university}`);
    }

    return {
        signIn: signIn,
        SiSC: SiSC,
        graduate: graduate
    }
}

Student.prototype.studentFactory1 = function(university) {
    function signIn() {
        console.log(`${this.name} ${this.surname} signed in to the university: ${university}`);
    }

    function SiSC() {
        if (university === 'ASE') {
            console.log(`${this.name} ${this.surname} is doing volunteering in SiSC organization.`);
        }
        else {
            console.log(`Too bad that ${this.name} ${this.surname} is studying at: ${university}. You can't join SiSC.`);
        }
    }

    function graduate() {
        console.log(`${this.name} ${this.surname} graduated university: ${university}`);
    }

    return {
        signIn: signIn.bind(this), // fac bind la this-ul curent
        SiSC: SiSC.bind(this),
        graduate: graduate.bind(this)
    }
}

Student.prototype.studentFactory2 = function(university) {
    var self = this;

    function signIn() {
        console.log(self);
        console.log(`${self.name} ${self.surname} signed in to the university: ${university}`);
    }

    function SiSC() {
        if (university === 'ASE') {
            console.log(`${self.name} ${self.surname} is doing volunteering in SiSC organization.`);
        }
        else {
            console.log(`Too bad that ${self.name} ${self.surname} is studying at: ${university}. You can't join SiSC.`);
        }
    }

    function graduate() {
        console.log(`${self.name} ${self.surname} graduated university: ${university}`);
    }

    return {
        signIn: signIn.bind(self), // fac bind la this-ul curent
        SiSC: SiSC.bind(self),
        graduate: graduate.bind(self)
    }
}

Student.prototype.studentFactory3 = function(university) {
    signIn = () => {
        console.log(`${this.name} ${this.surname} signed in to the university: ${university}`);
    }

    SiSC = () => {
        if (university === 'ASE') {
            console.log(`${this.name} ${this.surname} is doing volunteering in SiSC organization.`);
        }
        else {
            console.log(`Too bad that ${this.name} ${this.surname} is studying at: ${university}. You can't join SiSC.`);
        }
    }

    graduate = () => {
        console.log(`${this.name} ${this.surname} graduated university: ${university}`);
    }

    return {
        signIn: signIn,
        SiSC: SiSC,
        graduate: graduate
    }
}


var student = new Student('Jane','Doe',1075);
console.log(student);

student.studentFactory('ASE').signIn();
student.studentFactory1('ASE').SiSC();
student.studentFactory2('POLI').SiSC();
student.studentFactory3('ASE').graduate();