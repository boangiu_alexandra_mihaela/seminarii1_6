// Sugar syntax
class Person {
    constructor(name, surname) {
        this._name = name; // name fara _ -> cand apelam setter-ul, apelam stack over flow (s-ar fi apelat la infinit)
        this._surname = surname;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }
}

class Employee extends Person {
    constructor(name, surname, salary) {
        super(name, surname);
        this.salary = salary;
    }
}

var person = new Person('Gigescu', 'Popescu');
person.name = 'Gigel'; // accesul pe proprietate + operator = ii va spune interpretorului ca eu apelez setter-ul
console.log(person.name) // interpretorul va sti ca eu vreau sa apelex getter-ul

var emp1 = new Employee('John', 'Doe', 5000);
console.log(emp1);